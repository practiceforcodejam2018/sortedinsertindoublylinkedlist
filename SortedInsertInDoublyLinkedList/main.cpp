#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int data;
    struct node_structure *next;
    struct node_structure *previous;
}node_struct;

class DoublyLinkedList
{
    private:
        node_struct* createNodeWithData(int data)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data     = data;
                newNode->next     = NULL;
                newNode->previous = NULL;
            }
            return newNode;
        }
    
        node_struct* findAppropriateNodeInListWithHeadForInsertingNodeWithData(node_struct *head , int data)
        {
            node_struct *nodeForTraversal = head;
            while (nodeForTraversal->next)
            {
                if(nodeForTraversal->data >= data)
                {
                    return nodeForTraversal;
                }
                nodeForTraversal = nodeForTraversal->next;
            }
            return nodeForTraversal;
        }
    
    public:
        node_struct* createDoublyLinkedListInAscendingOrderFromVector(vector<int> listVector)
        {
            int         listVectorSize = (int)listVector.size();
            node_struct *head          = NULL;
            node_struct *tail          = NULL;
            node_struct *movingPointer = NULL;
            for(int i = 0 ; i < listVectorSize ; i++)
            {
                if(!head)
                {
                    head          = createNodeWithData(listVector[i]);
                    tail          = head;
                    movingPointer = head;
                }
                else
                {
                    node_struct *newNodeForInsertion = createNodeWithData(listVector[i]);
                    movingPointer                    = findAppropriateNodeInListWithHeadForInsertingNodeWithData(head , listVector[i]);
                    if(movingPointer->data >= listVector[i])
                    {
                        newNodeForInsertion->previous       = movingPointer->previous;
                        if(movingPointer->previous)
                        {
                            movingPointer->previous->next = newNodeForInsertion;
                        }
                        else
                        {
                            head = newNodeForInsertion;
                        }
                        newNodeForInsertion->next           = movingPointer;
                        movingPointer->previous             = newNodeForInsertion;
                    }
                    else if(movingPointer->next == NULL)
                    {
                        movingPointer->next           = newNodeForInsertion;
                        tail                          = newNodeForInsertion;
                        newNodeForInsertion->previous = movingPointer;
                    }
                }
            }
            tail->next     = head;
            head->previous = tail;
            return head;
        }
    
        void displayDoublyLinkedListWithHead(node_struct *head)
        {
            node_struct *movingHead = head;
            while (movingHead->next != head)
            {
                cout<<movingHead->data<<"->";
                movingHead = movingHead->next;
            }
            cout<<movingHead->data<<endl;
        }
};

int main(int argc, const char * argv[])
{
    vector<int>      listVector = {30 , 10 , 50 , 43 , 56 , 12};//{40 , 50 , 10 , 45 , 90 , 100 , 95};
    DoublyLinkedList dLL        = DoublyLinkedList();
    node_struct      *dLLHead   = dLL.createDoublyLinkedListInAscendingOrderFromVector(listVector);
    dLL.displayDoublyLinkedListWithHead(dLLHead);
    return 0;
}
